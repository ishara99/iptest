#!/bin/bash

IP=$1
TIME=$2
touch ping_service_${IP}.log

while sleep 1; do

dt=$(date '+%Y-%m-%d %H:%M:%S');

ping -c 1 $IP 2>/dev/null 1>/dev/null;
result=$?

if [ $result = 0 ]
then

#echo ${dt} Host reachable ${result} >> ping_service_${IP}.log

  t="$(ping -c 1 $IP | sed -ne '/.*time=/{;s///;s/\..*//;p;}')"

re='^[0-9]+$'
if ! [[ $t =~ $re ]] ; then
   #Not a number

   string=$t
   stringarray=($string)

   tnew=${stringarray[0]}
   if [ "$tnew" -gt $TIME ]; then
    echo ${dt} ${tnew} Higher ${result} ${TIME} >> ping_service_${IP}.log

   fi

else

  if [ "$t" -gt $TIME ]; then
    echo ${dt} ${t} Higher ${result} ${TIME} >> ping_service_${IP}.log
  fi
fi

elif [ $result = 1 ]
then
 echo ${dt} No reply ${result} ${TIME} >> ping_service_${IP}.log

else
 echo ${dt} Other error ${result} ${TIME} >> ping_service_${IP}.log
fi

done
