##
## ███████ ██ ██      ███████     ███████ ██    ██ ███    ██  ██████ 
## ██      ██ ██      ██          ██       ██  ██  ████   ██ ██      
## █████   ██ ██      █████       ███████   ████   ██ ██  ██ ██      
## ██      ██ ██      ██               ██    ██    ██  ██ ██ ██      
## ██      ██ ███████ ███████     ███████    ██    ██   ████  ██████ 
##  
## */2 * * * * /usr/Sync.sh                                                                
                                                                  
##Call Recording Files	
rsync -auopg -e ssh  /var/spool/asterisk/monitor/*  root@192.168.53.2:/var/spool/asterisk/monitor/

##Process Media (attachments) and Call Center Platform
rsync -auopg -e ssh  /var/www/html/dpmc_new_cc/*  root@192.168.53.2:/var/www/html/dpmc_new_cc/
rsync -auopg -e ssh /var/www/html/mail/*  root@192.168.53.2:/var/www/html/mail/
rsync -auopg  -e ssh /var/www/html/apis/*  root@192.168.53.2:/var/www/html/apis/
rsync -auopg -e ssh /var/www/html/Next\ Phone.exe  root@192.168.53.2:/var/www/html/ 
rsync -auopg  -e ssh /var/www/html/PhonikIPUpdate.exe  root@192.168.53.2:/var/www/html/
rsync -auopg -e ssh /etc/iphonik_ami/AMI_Event.php  root@192.168.53.2:/etc/iphonik_ami/

##AGI Scripts
rsync -auopg -e ssh /var/lib/asterisk/agi-bin/*  root@192.168.53.2:/var/lib/asterisk/agi-bin/

##Sound Files
rsync -auopg -e ssh /var/lib/asterisk/sounds/en/custom/*  root@192.168.53.2:/var/lib/asterisk/sounds/en/custom/
